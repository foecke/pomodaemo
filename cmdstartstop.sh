#!/bin/sh

cd "$(dirname "$0")"


if [ -f .pstarted ]; then
	if [ -f .ppaused ]; then
		echo `expr $(date +%s) - $(cat .ppaused) + $(cat .pstarted)` > .pstarted
		rm .ppaused
		notify-send 'unpaused'
	else
		echo $(date +%s) > .ppaused
		notify-send 'paused'
	fi
else
	if [ -f .ppaused ]; then
		rm .ppaused
	fi
	
	echo $(date +%s) > .pstarted
fi
