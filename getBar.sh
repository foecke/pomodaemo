#!/bin/sh

cd "$(dirname "$0")"

if [ -f .ppaused ]; then
	if [ -f .ppause ]; then
		curTime=`expr 300 - $(cat .ppaused) + $(cat .pstarted)`
		echo $((100*`date +%s -ud @${curTime}`/300))
	else
		curTime=`expr 1500 - $(cat .ppaused) + $(cat .pstarted)`
		echo $((100*`date +%s -ud @${curTime}`/1500))
	fi

elif [ -f .pstarted ]; then

	if [ -f .ppause ]; then
		curTime=`expr 300 - $(date +%s) + $(cat .pstarted)`
	else
		curTime=`expr 1500 - $(date +%s) + $(cat .pstarted)`
	fi

	if  (( curTime < 0)); then 
		echo 0
	else
		if [ -f .ppause ]; then
			echo $((100*`date +%s -ud @${curTime}`/300))
		else
			echo $((100*`date +%s -ud @${curTime}`/1500))
		fi
	fi

elif [ -f .pfinished ]; then

	echo 0

else

	echo 100

fi
