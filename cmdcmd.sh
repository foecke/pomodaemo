#!/bin/bash

cd "$(dirname "$0")"

if [ -f .ppause ]; then
	notify-send 'Pomodoro done!'
	$(feh -x -g 6000x2000 /home/lea/.scripts/blank.gif)
else
	notify-send 'Pause done!'
	$(feh -x -g 6000x2000 /home/lea/.scripts/blank.gif)
fi
