#!/bin/sh

cd "$(dirname "$0")"

if [ -f .ppause ]; then
	rm .ppause
else
	touch .ppause
fi
