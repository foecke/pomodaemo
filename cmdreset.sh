#!/bin/sh

cd "$(dirname "$0")"

if [ -f .pstarted ]; then 
	rm .pstarted
fi
if [ -f .pfinished ]; then 
	rm .pfinished
fi
if [ -f .ppaused ]; then 
	rm .ppaused
fi

