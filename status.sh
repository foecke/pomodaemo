#!/bin/sh

cd "$(dirname "$0")"

if [ -f .ppaused ]; then

	if [ -f .ppause ]; then
		curTime=`expr 300 - $(cat .ppaused) + $(cat .pstarted)`
	else
		curTime=`expr 1500 - $(cat .ppaused) + $(cat .pstarted)`
	fi
	echo `date +%M:%S -ud @${curTime}`

elif [ -f .pstarted ]; then

	if [ -f .ppause ]; then
		curTime=`expr 300 - $(date +%s) + $(cat .pstarted)`
	else
		curTime=`expr 1500 - $(date +%s) + $(cat .pstarted)`
	fi

	if  (( curTime < 0)); then 
		rm .pstarted
		touch .pfinished
		./cmdmode.sh
		./cmdcmd.sh
		echo done
	else
		echo `date +%M:%S -ud @${curTime}`
	fi

elif [ -f .pfinished ]; then

	echo done

else

	if [ -f .ppause ]; then
		echo `date +%M:%S -ud @300`
	else
		echo `date +%M:%S -ud @1500`
	fi

fi
